from django.urls import path
from category.api.views import Category, CategoryDetail, MostRequestedCategory, CategoryPriceRange, CategoryAverageServiceRadius, LeastUsedCategory, LeastRegisteredCategory, WorkersPerCategory,CategoryAverageCost, OtherCategory

app_name = "category"

urlpatterns = [
    path('', Category.as_view(), name="category"),
    path('most_requested/', MostRequestedCategory.as_view(), name="category_most_requested"),
    path('average_service_radius/', CategoryAverageServiceRadius.as_view(), name="average_service_radius"),
    path('price_range/<str:category_name>/', CategoryPriceRange.as_view(), name="category_price_range"),
    path('detail/<str:category_name>/', CategoryDetail.as_view(), name="category_detail"),
    path('least_requested/', LeastUsedCategory.as_view(), name="least_requested_category"),
    path('avgcost/', CategoryAverageCost.as_view(), name="average_cost_category"),
    path('least_registered/', LeastRegisteredCategory.as_view(), name="least_reguistered_workers_category"),
    path('workers_per_category/', WorkersPerCategory.as_view(), name="workers_per_category"),
    path('other_category/', OtherCategory.as_view(), name="other_category"),
]
