from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.core.validators import validate_comma_separated_integer_list
from rest_framework.authtoken.models import Token
from django.contrib.postgres.fields import ArrayField
from category.models import Category
from skill.models import Skill

# Create your models here.
class Account(AbstractUser):
    email = models.EmailField(unique=True)
    address_desc = models.CharField(max_length=50)
    address_lon = models.DecimalField(max_digits=9, decimal_places=6)
    address_lat = models.DecimalField(max_digits=9, decimal_places=6)
    name = models.CharField(max_length=75)
    is_worker = models.BooleanField(default=False)
    id_number = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=20)
    birth_date = models.DateField(null=True)
    date_created = models.DateTimeField(auto_now_add=True)

class Worker(models.Model):
    user = models.OneToOneField(Account, on_delete=models.CASCADE, null=True, related_name='worker_profile')
    SERVICE_RADIUS_CHOICES = [(i, i) for i in range(1,31)]
    service_radius = models.IntegerField(choices = SERVICE_RADIUS_CHOICES)
    price_per_hour = models.DecimalField(max_digits=6, decimal_places=2)
    ratings = models.CharField(max_length=100000, validators=[validate_comma_separated_integer_list], null=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, null=True, editable=True)
    category = models.ManyToManyField(Category)
    skill = models.ManyToManyField(Skill)
    bio = models.CharField(max_length=500, null=True, blank=True)
    profile_picture = models.URLField(max_length=999)
    previous_jobs_img = ArrayField(models.URLField(max_length=999))

    def clean(self, rate):
        if self.rating == None:
            self.rating = int(rate)
        else:
            ratings = eval(self.ratings)
            rating = self.rating
            n = len(ratings)
            self.rating = rating + ((int(rate)-rating)/(n+1))
        if self.ratings == None:
            self.ratings = '[' + str(rate) + ',]'
        else:
            beggining_substring = self.ratings[:-1]
            ending_substring = self.ratings[-1]
            self.ratings = beggining_substring + (str(rate) + ',') + ending_substring
        self.save()

class Client(models.Model):
    user = models.OneToOneField(Account, on_delete=models.CASCADE, null=True, related_name='user_profile')
    favorites = models.ManyToManyField(Worker)
    
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)