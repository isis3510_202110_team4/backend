import json
from rest_framework import serializers
from account.models import Account, Client, Worker
from category.models import Category
from skill.models import Skill


class RegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account

        fields = ['username', 'is_worker', 'email', 'name', 'id_number',
                  'address_desc', 'address_lon', 'address_lat',
                  'phone_number', 'birth_date', 'password',
                 ]

    def save(self):
        user = Account(
            username=self.validated_data['username'],
            is_worker=self.validated_data['is_worker'],
            email=self.validated_data['email'],
            name=self.validated_data['name'],
            id_number=self.validated_data['id_number'],
            address_desc = self.validated_data['address_desc'],
            address_lon = self.validated_data['address_lon'],
            address_lat = self.validated_data['address_lat'],
            phone_number = self.validated_data['phone_number'],
            birth_date = self.validated_data['birth_date'],
        )

        password = self.validated_data['password']
        user.set_password(password)
        user.save()
        return user

class ClientSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField('get_client')
    favorites = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Client
        fields = ['id', 'user', 'favorites']

    def get_client(self, client):
        client = client.user.email
        return client

class WorkerSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField('get_worker')
    rating = serializers.ReadOnlyField()
    category = serializers.PrimaryKeyRelatedField(many=True, queryset = Category.objects.all())
    skill = serializers.PrimaryKeyRelatedField(many=True, queryset = Skill.objects.all())
    previous_jobs_img = serializers.ListField(child=serializers.URLField())
    bio = serializers.CharField(allow_null=True, allow_blank=True)

    class Meta:
        model = Worker
        fields = ['id', 'user', 'service_radius', 'price_per_hour', 'rating', 'category', 'skill', 'profile_picture', 'previous_jobs_img', 'bio']
    
    def get_worker(self, worker):
        worker = worker.user.email
        return worker

class UserSerializer(serializers.ModelSerializer):
    date_created = serializers.ReadOnlyField()

    class Meta:
        model = Account
        fields = ['id', 'email', 'username', 'is_worker', 'name', 'id_number', 'address_desc', 'address_lon', 'address_lat', 'phone_number', 'birth_date', 'date_created']

class WorkersSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField('get_worker_email')
    username = serializers.SerializerMethodField('get_worker_username')
    id_number = serializers.SerializerMethodField('get_worker_id_number')
    name = serializers.SerializerMethodField('get_worker_name')
    address_desc = serializers.SerializerMethodField('get_worker_address_desc')
    address_lon = serializers.SerializerMethodField('get_worker_address_lon')
    address_lat = serializers.SerializerMethodField('get_worker_address_lat')
    phone_number = serializers.SerializerMethodField('get_worker_phone_number')
    birth_date = serializers.SerializerMethodField('get_worker_birth_date')
    date_created = serializers.SerializerMethodField('get_worker_date_created')
    
    category = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    skill = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    previous_jobs_img = serializers.ListField(child=serializers.URLField(read_only=True), read_only=True)
    bio = serializers.CharField(allow_null=True, allow_blank=True)

    class Meta:
        model = Worker
        fields = ['id', 'username', 'email', 'name', 'id_number', 'address_desc', 'address_lon', 'address_lat', 'phone_number', 'birth_date', 'date_created', 'service_radius', 'price_per_hour', 'rating', 'category', 'skill', 'profile_picture', 'previous_jobs_img', 'bio']

    def get_worker_email(self, worker):
        worker = worker.user.email
        return worker

    def get_worker_username(self, worker):
        worker = worker.user.username
        return worker

    def get_worker_name(self, worker):
        worker = worker.user.name
        return worker

    def get_worker_id_number(self, worker):
        worker = worker.user.id_number
        return worker

    def get_worker_address_desc(self, worker):
        worker = worker.user.address_desc
        return worker

    def get_worker_address_lon(self, worker):
        worker = worker.user.address_lon
        return worker

    def get_worker_address_lat(self, worker):
        worker = worker.user.address_lat
        return worker

    def get_worker_phone_number(self, worker):
        worker = worker.user.phone_number
        return worker

    def get_worker_birth_date(self, worker):
        worker = worker.user.birth_date
        return worker

    def get_worker_date_created(self, worker):
        worker = worker.user.date_created
        return worker

class ClientsSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField('get_client_email')
    username = serializers.SerializerMethodField('get_client_username')
    id_number = serializers.SerializerMethodField('get_client_id_number')
    name = serializers.SerializerMethodField('get_client_name')
    address_desc = serializers.SerializerMethodField('get_client_address_desc')
    address_lon = serializers.SerializerMethodField('get_client_address_lon')
    address_lat = serializers.SerializerMethodField('get_client_address_lat')
    phone_number = serializers.SerializerMethodField('get_client_phone_number')
    birth_date = serializers.SerializerMethodField('get_client_birth_date')
    date_created = serializers.SerializerMethodField('get_client_date_created')

    favorites = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Client
        fields = ['id', 'username', 'email', 'name', 'id_number', 'address_desc', 'address_lon', 'address_lat', 'phone_number', 'birth_date', 'date_created', 'favorites']

    def get_client_email(self, client):
        client = client.user.email
        return client

    def get_client_username(self, client):
        client = client.user.username
        return client

    def get_client_name(self, client):
        client = client.user.name
        return client

    def get_client_id_number(self, client):
        client = client.user.id_number
        return client

    def get_client_address_desc(self, client):
        client = client.user.address_desc
        return client

    def get_client_address_lon(self, client):
        client = client.user.address_lon
        return client

    def get_client_address_lat(self, client):
        client = client.user.address_lat
        return client

    def get_client_phone_number(self, client):
        client = client.user.phone_number
        return client

    def get_client_birth_date(self, client):
        client = client.user.birth_date
        return client

    def get_client_date_created(self, client):
        client = client.user.date_created
        return client