from django.urls import path
from account.api.views import UserCreateAPIView, UserList, UserDetail, WorkerDetail, ClientDetail, Worker, MessageAPI, Client, WorkerServicesCompleted, ClientFavoriteCategory, WorkerServicesCompletedDetail, HigherRatedWorkerCategory, HigherWorkersCategoryMonth, ClosestWorkers, ClientAddFavoriteWorker, ClientDeleteFavoriteWorker, WorstRatedWorker, JobsCompletedByWorker, ClientFavoriteWorkers, WorkerNumFavorites, ClientReuquestFrequency, ClientServices, CurrentUser, HighestWorkersClientFavoriteCategory
from rest_framework.authtoken.views import obtain_auth_token

app_name = "account"

urlpatterns = [
    path('register/', UserCreateAPIView.as_view(), name="register"),
    path('login/', obtain_auth_token, name="register"),
    path('list/', UserList.as_view(), name="list_users"),
    path('messages/', MessageAPI.as_view(), name ="send_message"),
    path('client/', Client.as_view(), name = 'clients'),
    path('current-user/', CurrentUser.as_view(), name="current_user"),
    path('client/detail/<str:client_id>/', ClientDetail.as_view(), name = 'client_detail'),
    path('client/detail/<str:client_id>/services/', ClientServices.as_view(), name = 'client_services'),
    path('client/detail/<str:client_id>/favorite_workers/', ClientFavoriteWorkers.as_view(), name = 'client_favorite_workers'),
    path('client/detail/<str:client_id>/request_frequency/', ClientReuquestFrequency.as_view(), name = 'client_request_frequency'),
    path('client/detail/<str:client_id>/favorite_category/', ClientFavoriteCategory.as_view(), name = 'client_favorite_category'),
    path('client/detail/<str:client_id>/favorite_category_workers/', HighestWorkersClientFavoriteCategory.as_view(), name = 'client_favorite_category_workers'),
    path('client/detail/<str:client_id>/add_favorite/<str:worker_id>/', ClientAddFavoriteWorker.as_view(), name = 'client_add_favorite_worker'),
    path('client/detail/<str:client_id>/delete_favorite/<str:worker_id>/', ClientDeleteFavoriteWorker.as_view(), name = 'client_delete_favorite_worker'),
    path('worker/', Worker.as_view(), name = 'workers'),
    path('worker/closest/<str:client_id>/', ClosestWorkers.as_view(), name = 'closest_workers'),
    path('worker/detail/<str:worker_id>/', WorkerDetail.as_view(), name = 'worker_detail'),
    path('worker/detail/<str:worker_id>/num_favorites/', WorkerNumFavorites.as_view(), name = 'worker_num_favorites'),
    path('worker/detail/<str:worker_id>/services_completed/', WorkerServicesCompleted.as_view(), name = 'worker_services_completed'),
    path('worker/detail/<str:worker_id>/services_completed/detail/', WorkerServicesCompletedDetail.as_view(), name = 'worker_services_completed_detail'),
    path('detail/<str:user_id>/', UserDetail.as_view(), name = 'user_detail'),
    path('worker/best_by_category/', HigherRatedWorkerCategory.as_view(), name='higher_rated_category'),
    path('worker/best_by_category_month/', HigherWorkersCategoryMonth.as_view(), name='higher_rated_category_month'),
    path('worker/worst_by_category/', WorstRatedWorker.as_view(), name='worst_rated_category'),
    path('worker/completed_per_worker', JobsCompletedByWorker.as_view(), name='completed_per_worker'),
]