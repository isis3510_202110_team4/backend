from django.conf import LazySettings
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Avg, F
from rest_framework import response
from rest_framework.views import APIView
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes
from datetime import date, timedelta, datetime
from services_backend.twilio_api import sendMessage 
from account.api.serializers import RegistrationSerializer, UserSerializer, WorkerSerializer, ClientSerializer, WorkersSerializer, ClientsSerializer
from account import models
from service.models import Service
from service.api.serializers import ServiceSerializer
from category.models import Category
from category.api.serializers import CategorySerializer
import geopy.distance
import json

class UserCreateAPIView(APIView):
    def post(self, request):
        serializer1 = RegistrationSerializer(data=request.data)
        if serializer1.is_valid():
            if request.data.get('is_worker')=='True':
                serializer2 = WorkerSerializer(data=request.data)
                if serializer2.is_valid():
                    serializer1.save()
                    user_email = request.data.get('email')
                    try:
                        serializer2.save(user=models.Account.objects.get(email=user_email))
                        context = {'General information': serializer1.data, 'Specific information': serializer2.data}
                    except:
                        user=models.Account.objects.get(email=user_email)
                        user.delete()
                        return Response(serializer2.errors, status=status.HTTP_400_BAD_REQUEST)
                    return Response(context, status=status.HTTP_201_CREATED)
                return Response(serializer2.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                serializer2 = ClientSerializer(data=request.data)
                if serializer2.is_valid():
                    serializer1.save()
                    user_email = request.data.get('email')
                    serializer2.save(user=models.Account.objects.get(email=user_email))
                    context = {'General information': serializer1.data, 'Specific information': serializer2.data}
                    return Response(context, status=status.HTTP_201_CREATED)
                return Response(serializer2.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer1.errors, status=status.HTTP_400_BAD_REQUEST)

class UserList(APIView):
    def get(self, request):
        users = models.Account.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

class MessageAPI(APIView):
    def post(self, request):
        sendMessage(request.data["number"], request.data["message"])
        return Response({"se envio el mensaje: " + request.data["message"] + "correctamente al numero: " + request.data["number"]})

class UserDetail(APIView):
    def get(self, request, user_id):
        try:
            user = models.Account.objects.get(pk=user_id)
        except models.Account.DoesNotExist:
            raise Http404
        serializer = UserSerializer(user)
        return Response(serializer.data)

class Worker(APIView):
    def get(self, request):
        workers = models.Worker.objects.all()
        serializer = WorkersSerializer(workers, many=True)
        return Response(serializer.data)

class WorkerDetail(APIView):
    def get(self, request, worker_id):
        try:
            worker = models.Worker.objects.get(pk=worker_id)
        except Worker.DoesNotExist:
            raise Http404
        serializer = WorkersSerializer(worker)
        return Response(serializer.data)

    def patch(self, request, worker_id):
        try:
            worker = models.Worker.objects.get(pk=worker_id)
        except models.Worker.DoesNotExist:
            raise Http404

        serializer = WorkerSerializer(worker, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class WorkerServicesCompleted(APIView):
    def get(self, request, worker_id):
        try:
            worker = models.Worker.objects.get(pk=worker_id)
            services_completed = Service.objects.filter(worker=worker_id, in_progress=False).count()
        except Worker.DoesNotExist:
            raise Http404
        data = {}
        serializer = WorkersSerializer(worker)
        data.update(serializer.data)
        data['Services Completed'] = services_completed
        return Response(data)

class WorkerServicesCompletedDetail(APIView):
    def get(self, request, worker_id):
        try:
            worker = models.Worker.objects.get(pk=worker_id)
            services_completed = Service.objects.filter(worker=worker_id, in_progress=False)
        except Worker.DoesNotExist:
            raise Http404
        data = {}
        serializer = WorkersSerializer(worker)
        serializer2 = ServiceSerializer(services_completed, many=True)
        data['Worker'] = serializer.data
        data['Services Completed'] = serializer2.data
        return Response(data)

class Client(APIView):
    def get(self, request):
        clients = models.Client.objects.all()
        serializer = ClientsSerializer(clients, many=True)
        return Response(serializer.data)

class ClientDetail(APIView):
    def get(self, request, client_id):
        try:
            client = models.Client.objects.get(pk=client_id)
        except models.Client.DoesNotExist:
            raise Http404
        serializer = ClientSerializer(client)
        return Response(serializer.data)

    def patch(self, request, client_id):
        try:
            client = models.Client.objects.get(pk=client_id)
        except models.Client.DoesNotExist:
            raise Http404

        serializer = ClientSerializer(client, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

class ClientServices(APIView):
    def get(self, request, client_id):
        try:
            services = Service.objects.filter(client=client_id)
        except Service.DoesNotExist:
            raise Http404
        serializer = ServiceSerializer(services, many=True)
        return Response(serializer.data)

class HigherRatedWorkerCategory(APIView):
    def get(self, request):
        categories = Category.objects.all()
        answer_list = []
        for category in categories:
            worker = models.Worker.objects.filter(category=category).order_by('-rating').first()
            print(worker)
            if worker is not None:
                answer_list.append(worker)
            else:
                continue
        serializer = WorkersSerializer(answer_list, many=True)
        return Response(serializer.data)
        
class WorstRatedWorker(APIView):
    def get(self, request):
        categories = Category.objects.all()
        answer_list = []
        for category in categories:
            worker = models.Worker.objects.filter(category=category).order_by('rating').first() 
            if worker is not None:
                answer_list.append(worker)
            else:
                continue
        serializer = WorkerSerializer(answer_list, many=True)
        return Response(serializer.data)

class HigherWorkersCategoryMonth(APIView):
    def get(self, request):
        categories = Category.objects.all()
        workers_month = {}
        start_date = date.today() - timedelta(30)
        end_date = date.today()
        
        for category in categories:
            workers_month[category.name]={}
            try:
                workers = models.Worker.objects.filter(category=category)
                highest_rating = 0
                worker_category = {}
                for worker in workers:
                    try:
                        rating_month = Service.objects.filter(date_scheduled__range=(start_date, end_date), in_progress=False, worker=worker.id).aggregate(Avg('rating'))
                        if rating_month.get('rating__avg') > highest_rating:
                            serializer = WorkersSerializer(worker)
                            worker_category = serializer.data
                    except:
                        continue
                workers_month[category.name]=worker_category
            except:
                continue
        return Response(workers_month)

class ClientFavoriteCategory(APIView):
    def get(self, request, client_id):
        categories = models.Category.objects.all()
        most_requested_category = None
        most_jobs_requested = 0
        for category in categories:
            try:
                jobs_requested = Service.objects.filter(category=category, client=client_id).count()
                if most_jobs_requested < jobs_requested:
                    most_jobs_requested = jobs_requested
                    most_requested_category = category
                else:
                    pass
            except:
                continue
        serializer = CategorySerializer(most_requested_category)
        return Response(serializer.data)

class ClosestWorkers(APIView):
    def get(self, request, client_id):
        client = models.Client.objects.get(pk=client_id).user
        coordinates1 = (client.address_lat, client.address_lon)
        workers = models.Worker.objects.all()
        closest_workers=[]
        for worker in workers:
            radius = worker.service_radius
            coordinates2 = (worker.user.address_lat, worker.user.address_lon)
            distance = geopy.distance.distance(coordinates1, coordinates2).km
            if distance <= radius:
                closest_workers.append(worker)
            else:
                continue
        serializer = WorkersSerializer(closest_workers, many=True)
        return Response(serializer.data)
            
class ClientAddFavoriteWorker(APIView):
    def post(self, request, client_id, worker_id):
        try:
            client = models.Client.objects.get(pk=client_id)
            try:
                worker = models.Worker.objects.get(pk=worker_id)
            except models.Worker.DoesNotExist:
                raise Http404
            client.favorites.add(worker)
        except models.Client.DoesNotExist:
            raise Http404
        serializer = ClientsSerializer(client)
        return Response(serializer.data)
            
class ClientDeleteFavoriteWorker(APIView):
    def post(self, request, client_id, worker_id):
        try:
            client = models.Client.objects.get(pk=client_id)
            try:
                worker = models.Worker.objects.get(pk=worker_id)
            except models.Worker.DoesNotExist:
                raise Http404
            client.favorites.remove(worker)
        except models.Client.DoesNotExist:
            raise Http404
        serializer = ClientsSerializer(client)
        return Response(serializer.data)

class JobsCompletedByWorker(APIView):
    def get(self, request):
        workers = models.Worker.objects.all()
        answer=[]
        for worker in workers:
            try:
                completed_services = Service.objects.get(worker=worker, in_progress=False, cancelled=False).count()
                answer_item = {"id":worker.id,"name":worker.name, "completed":completed_services}
                answer.append(answer_item)
            except:
                continue
        return Response(answer)
        
class ClientFavoriteWorkers(APIView):
    def get(self, request, client_id):
        try:
            workers = models.Worker.objects.filter(client__id=client_id)
        except models.Worker.DoesNotExist:
            raise Http404
        serializer = WorkersSerializer(workers, many=True)
        return Response(serializer.data)

class WorkerNumFavorites(APIView):
    def get(self, request, worker_id):
        try:
            worker = models.Worker.objects.get(pk=worker_id)
            num_favorites=worker.client_set.all().count()
            data={'Favorites': num_favorites}
        except models.Worker.DoesNotExist:
            raise Http404
        return Response(data)
        
class ClientReuquestFrequency(APIView):
    def get(self, request, client_id):
        try:
            services = Service.objects.filter(client=client_id).order_by('date_scheduled')
            frequency_total = timedelta(0)
            for i in range(0, len(services)-1):
                diff = services[i+1].date_scheduled - services[i].date_scheduled
                frequency_total += diff
        except Service.DoesNotExist:
            raise Http404
        if len(services) <= 0:
                raise Http404
        frequency_average = timedelta(frequency_total/timedelta(len(services)))
        return Response({'Frequency': frequency_average, 'Units': 'seconds'})

class CurrentUser(APIView):
    def get(self, request):
        permission_classes = (IsAuthenticated,)
        current_user = request.user
        user = UserSerializer(current_user)
        current_user_id = user.data['id']
        current_client = models.Client.objects.get(user=current_user_id)
        serializer = ClientsSerializer(current_client)
        return Response(serializer.data)


class HighestWorkersClientFavoriteCategory(APIView):
    def get(self, request, client_id):
        categories = models.Category.objects.all()
        most_requested_category = None
        most_jobs_requested = 0
        for category in categories:
            try:
                jobs_requested = Service.objects.filter(category=category, client=client_id).count()
                if most_jobs_requested < jobs_requested:
                    most_jobs_requested = jobs_requested
                    most_requested_category = category
                else:
                    pass
            except:
                continue
        try:
            workers_orderd = models.Worker.objects.filter(category=most_requested_category).order_by(F('rating').desc(nulls_last=True))
            serializer = WorkersSerializer(workers_orderd, many=True)
        except models.Worker.DoesNotExist:
            raise Http404

        return Response(serializer.data)