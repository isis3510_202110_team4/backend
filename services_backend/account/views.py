from django.shortcuts import render
from account.models import Worker

# Create your views here.
def add_and_calculate_rating(rate, worker_id):
    worker = Worker.objects.get(id=worker_id)
    if worker.rating == None:
        worker.rating = int(rate)
    else:
        print(worker.ratings)
        ratings = eval(worker.ratings)
        rating = worker.rating
        n = len(ratings)
        worker.rating = rating + ((n*rating+int(rate))/(n+1))
    if worker.ratings == None:
        worker.ratings = '[' + str(rate) + ',]'
    else:
        beggining_substring = worker.ratings[:-2]
        ending_substring = worker.ratings[-1]
        worker.ratings = beggining_substring + (str(rate) + ',') + ending_substring
