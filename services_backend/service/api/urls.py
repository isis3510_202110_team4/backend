from django.urls import path
from service.api.views import Service, ServiceDetail

app_name = "service"

urlpatterns = [
    path('', Service.as_view(), name="service"),
    path('detail/<str:service_id>/', ServiceDetail.as_view(), name="service_detail"),
]
