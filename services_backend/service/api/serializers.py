from rest_framework import serializers
from service import models


class ServiceSerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField('get_service_client')
    worker = serializers.SerializerMethodField('get_service_worker')
    category = serializers.SerializerMethodField('get_service_category')
    source = serializers.CharField(allow_null=True)
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Service
        fields = ['id', 'client', 'worker', 'category', 'address_lon', 'address_lat', 'rating', 'in_progress', 'desc', 'date_scheduled', 'date_finished', 'price', 'cancelled', 'payment_method', 'source']

    def get_service_client(self, service):
        service_client = service.client.id
        return service_client

    def get_service_worker(self, service):
        service_worker = service.worker.id
        return service_worker

    def get_service_category(self, service):
        service_category = service.category.name
        return service_category