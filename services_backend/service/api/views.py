from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.parsers import FormParser, MultiPartParser
from service.api import serializers
from service import models
from account.models import Worker, Client
from category.models import Category
from account.views import add_and_calculate_rating

class Service(APIView):
    def get(self, request):
        services = models.Service.objects.all()
        serializer = serializers.ServiceSerializer(services, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.ServiceSerializer(data=request.data)
        if serializer.is_valid():
            try:
                worker_id = request.data.get('worker')
                client_id = request.data.get('client')
                category_name = request.data.get('category')
                serializer.save(client = Client.objects.get(id=client_id), worker = Worker.objects.get(id=worker_id), category = Category.objects.get(name=category_name))
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except:
                return Response(serializer.errors, status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ServiceDetail(APIView):
    def get(self, request, service_id):
        try:
            service = models.Service.objects.get(pk=service_id)
        except models.Service.DoesNotExist:
            raise Http404
        serializer = serializers.ServiceSerializer(service)
        return Response(serializer.data)

    def put(self, request, service_id):
        try:
            service = models.Service.objects.get(pk=service_id)
        except models.Service.DoesNotExist:
            raise Http404

        serializer = serializers.ServiceSerializer(service, data=request.data)
        if serializer.is_valid():
            worker = Worker.objects.get(id=request.data.get('worker'))
            worker.clean(rate=request.data.get('rating'))
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, service_id):
        try:
            service = models.Service.objects.get(pk=service_id)
        except models.Service.DoesNotExist:
            raise Http404
        service.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
