from django.db import models
from account.models import Client, Worker
from category.models import Category

# Create your models here.

class Service(models.Model):
    client = models.ForeignKey(Client, on_delete = models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete = models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    address_lon = models.DecimalField(max_digits=9, decimal_places=6)
    address_lat = models.DecimalField(max_digits=9, decimal_places=6)
    RATING_CHOICES = [(i, i) for i in range(1,6)]
    rating = models.IntegerField(choices = RATING_CHOICES, null=True)
    in_progress = models.BooleanField(default=True)
    desc = models.CharField(max_length=300)
    date_scheduled = models.DateTimeField()
    date_finished = models.DateTimeField(null=True)
    price = models.FloatField(null=True)
    CASH = 'CH'
    CREDIT = 'CT'
    DEBIT = 'DT'
    ONLINE = 'OE'
    PAY_CHOICES = [(CASH, 'Cash'), (CREDIT, 'Credit Card'), (DEBIT, 'Debit Card'), (ONLINE, 'Online')]
    payment_method = models.CharField(choices=PAY_CHOICES, max_length=50)
    MAP = 'MP'
    SEARCH = 'SH'
    SOURCE_CHOICES = [(MAP, 'Map'), (SEARCH, 'Search')]
    source = models.CharField(choices=SOURCE_CHOICES, max_length=50, null=True)
    cancelled = models.BooleanField(default=False)

    class Meta:
        constraints = [models.UniqueConstraint(fields=['worker', 'date_scheduled'], name='unique_booking'),]