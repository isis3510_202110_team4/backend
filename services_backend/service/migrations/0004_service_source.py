# Generated by Django 3.1.7 on 2021-06-04 20:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0003_auto_20210603_1704'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='source',
            field=models.CharField(choices=[('MP', 'Map'), ('SH', 'Search')], max_length=50, null=True),
        ),
    ]
