from django.db import models
from account.models import Client, Worker

# Create your models here.

class Survey(models.Model):
    client = models.ForeignKey(Client, on_delete = models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete = models.CASCADE)
    SKILLS = 'SK'
    RATING = 'RT'
    BIOGRAPHY = 'BG'
    PREVIOUS_WORKS = 'PW'
    PERSONAL_RECOMMENDATIONS = 'PR'
    SURVEY_CHOICES = [(SKILLS, 'Skills'), (RATING, 'Rating'), (BIOGRAPHY, 'Biography'), (PREVIOUS_WORKS, 'Previous works'), (PERSONAL_RECOMMENDATIONS, 'Personal Recommendations')]
    options = models.CharField(choices=SURVEY_CHOICES, max_length=50)