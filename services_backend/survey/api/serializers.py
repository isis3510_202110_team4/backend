from rest_framework import serializers
from survey import models

class SurveySerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField('get_survey_client')
    worker = serializers.SerializerMethodField('get_survey_worker')
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Survey

        fields = ['id', 'client', 'worker', 'options']

    def get_survey_client(self, survey):
        survey_client = survey.client.id
        return survey_client

    def get_survey_worker(self, survey):
        survey_worker = survey.worker.id
        return survey_worker