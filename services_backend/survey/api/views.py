from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
from django.db.models import Min, Max, Avg
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.parsers import FormParser, MultiPartParser
from survey.api import serializers
from survey import models
from account.models import Client, Worker
import json


class Survey(APIView):
    def get(self, request):
        surveys = models.Survey.objects.all()
        serializer = serializers.SurveySerializer(surveys, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.SurveySerializer(data=request.data)
        if serializer.is_valid():
            try:
                worker_id = request.data.get('worker')
                client_id = request.data.get('client')
                serializer.save(client = Client.objects.get(id=client_id), worker=Worker.objects.get(id=worker_id))
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except:
                return Response(serializer.data, status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SurveyDetail(APIView):
    def get(self, request, survey_id):
        try:
            survey = models.Survey.objects.get(pk=survey_id)
        except models.Survey.DoesNotExist:
            raise Http404
        serializer = serializers.SurveySerializer(survey)
        return Response(serializer.data)

    def put(self, request, survey_id):
        try:
            survey = models.Survey.objects.get(pk=survey_id)
        except models.Survey.DoesNotExist:
            raise Http404

        serializer = serializers.SurveySerializer(survey, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, survey_id):
        try:
            survey = models.Survey.objects.get(pk=survey_id)
        except models.Survey.DoesNotExist:
            raise Http404
        survey.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)