from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
from django.db.models import Min, Max, Avg
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.parsers import FormParser, MultiPartParser
from skill.api import serializers
from skill import models
from service.models import Service
from account.models import Worker
import json


class Skill(APIView):
    def get(self, request):
        skills = models.Skill.objects.all()
        serializer = serializers.SkillSerializer(skills, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.SkillSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SkillDetail(APIView):
    def get(self, request, skill_name):
        try:
            skill = models.Skill.objects.get(pk=skill_name)
        except models.Skill.DoesNotExist:
            raise Http404
        serializer = serializers.SkillSerializer(skill)
        return Response(serializer.data)

    def put(self, request, skill_name):
        try:
            skill = models.Skill.objects.get(pk=skill_name)
        except models.Skill.DoesNotExist:
            raise Http404

        serializer = serializers.SkillSerializer(skill, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, skill_name):
        try:
            skill = models.Skill.objects.get(pk=skill_name)
        except models.Skill.DoesNotExist:
            raise Http404
        skill.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# class MostRequestedCategory(APIView):
#     def get(self, request):
#         categories = models.Category.objects.all()
#         most_requested_category = None
#         most_jobs_requested = 0
#         for category in categories:
#             try:
#                 jobs_requested = Service.objects.filter(category=category).count()
#                 if most_jobs_requested < jobs_requested:
#                     most_jobs_requested = jobs_requested
#                     most_requested_category = category
#                 else:
#                     pass
#             except models.Category.DoesNotExist:
#                 raise Http404
#         serializer = serializers.CategorySerializer(most_requested_category)
#         return Response(serializer.data)

# class CategoryPriceRange(APIView):
#     def get(self, request, category_name):
#         try:
#             category = models.Category.objects.get(pk=category_name)
#             max_price = Worker.objects.filter(category=category_name).aggregate(Max('price_per_hour'))
#             min_price = Worker.objects.filter(category=category_name).aggregate(Min('price_per_hour'))
#         except models.Category.DoesNotExist:
#             raise Http404
#         serializer = serializers.CategorySerializer(category)
#         data = serializer.data
#         data.update(max_price)
#         data.update(min_price)
#         return Response(data)

# class CategoryAverageServiceRadius(APIView):
#     def get(self, request):
#         data = {}
#         categories = models.Category.objects.all()
#         for category in categories:
#             average_service_radius = Worker.objects.filter(category=category).aggregate(Avg('service_radius'))
#             data[category.name] = average_service_radius
#         return Response(data)

# class LeastUsedCategory(APIView):
#     def get(self, request):
#         categories = models.Category.objects.all()
#         least = Service.objects.filter(category=categories[0]).count()
#         least_name = categories[0].name
#         for category in categories:
#             current = Service.objects.filter(category=category).count()
#             if current < least:
#                 least_name = category.name
#                 least = current
#         return Response({least_name}) 

# class CategoryAverageCost(APIView):
#     def get(self, request):
#         categories = models.Category.objects.all()
#         averge_per_category = []
#         for category in categories:
#             services_category = Service.objects.filter(category=category)
#             avg = 0
#             for service in services_category:
#                 if service.price is not None:
#                     avg += service.price
#             averge_per_category.append({"category":category.name, "average price":avg})
#         return Response(averge_per_category)

# class LeastRegisteredCategory(APIView):
#     def get(self, request):
#         categories = models.Category.objects.all()
#         least = Service.objects.filter(category=categories[0]).count()
#         least_name = categories[0].name
#         for category in categories:
#             current = Worker.objects.filter(category=category).count()
#             if current < least:
#                 least_name = category.name
#                 least = current
#         return Response({least_name})

# class WorkersPerCategory(APIView):
#     def get(self, request):
#         categories = models.Category.objects.all()
#         workers_per_category = []
#         for category in categories:
#             workers_category = Worker.objects.filter(category=category).count()
#             workers_per_category.append({"category":category.name, "amount of workers":workers_category})
#         return Response(workers_per_category)
