from rest_framework import serializers
from skill import models

class SkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Skill

        fields = ['name', 'desc']