from django.db import models

# Create your models here.

class Skill(models.Model):
    name = models.CharField(max_length=50, unique=True, primary_key=True)
    desc = models.CharField(max_length=300)